var express = require('express')
var app = express()

var nodes = [
   { id: 1, name: 'nodename-1' , pid: 2 },
   { id: 2, name: 'nodename-2', pid: null },
   { id: 3, name: 'nodename-3', pid: 2 },
   { id: 4, name: 'nodename-4' ,pid: null },
   { id: 55,name: 'nodename-55' , pid: 4 },
   { id: 67, name: 'nodename-67', pid: 4 },
   { id: 11, name: 'nodename-11', pid: 10 },
   { id: 10, name: 'nodename-10' , pid: 4 },
   { id: 32, name: 'nodename-32' , pid: 11 },
   { id: 31, name: 'nodename-31' , pid: 10 },
   { id: 33, name: 'nodename-33' , pid: 32 },
   { id: 7, name: 'nodename-7', pid: null }
]

app.use(express.static(__dirname + '/app'));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));

app.get('/nodes', function (req, res) {
  res.send(nodes)
})

app.get('/', function(req, res){
	res.type('html').sendFile(__dirname + '/index.html');
});

app.listen(3000, function () {
  console.log('App is now up in port 3000')
})
