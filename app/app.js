var gigaView = angular.module('gigaView', ['treeControl', 'ngPrettyJson']);

gigaView.controller('ctrl', ['$scope', '$http', function ($scope, $http) {

  // Declaring variables for later use
  var nodes, finalTree = []
  var temp = {}

  // Getting nodes
  $http({
		method: 'GET',
		url: '/nodes'
	}).then(function successCallback(response) {
    nodes = response.data;
    // Process the nodes into desired tree
    unFlat()
	});

  function unFlat() {
    for (var i in nodes) {
      // If the node id is already registered in the temp dictonary, add its name and id and create children array
      if (temp[nodes[i].id]) {
        curNode = temp[nodes[i].id]
        curNode.name = nodes[i].name
        // Else, create the whole thing without pid, and add children array in it
      } else {
        curNode = {
            'id': nodes[i].id,
            'name': nodes[i].name,
            'children': [],
        }
        // Add the result to temp
        temp[nodes[i].id] = curNode
      }

      // If the current node has pid
      if (nodes[i].pid) {
        // If the node's parent is already registered in the temp dictonary - take it as it is
        if (temp[nodes[i].pid]) {
          var parent = temp[nodes[i].pid]
          // Else, create a parent object with the node's pid and add children array
        } else {
          parent = {
              'id': nodes[i].pid,
              'children': [],
          }
          // Add the result to temp as a parent node
          temp[nodes[i].pid] = parent
        }
        // Push the node to its parent
        parent.children.push(curNode)
        // Else (If the current node has no pid), push the node to the final tree (its a root node)
      } else {
        finalTree.push(curNode)
      }
    }
    // When done save the tree in the scope
    if (i == nodes.length-1) {
      $scope.nodes = finalTree
    }
  }

  $scope.showSelected = function(sel) {
    $scope.selectedNode = sel;
  };

}]);
